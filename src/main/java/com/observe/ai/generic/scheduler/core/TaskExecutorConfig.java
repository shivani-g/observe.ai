package com.observe.ai.generic.scheduler.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class TaskExecutorConfig {

    @Autowired
    private TaskHandlerFactory taskHandlerFactory;


    /**
     * Initializes 2 threads for tasks execution
     * @return
     */
    @Bean(destroyMethod = "shutdown")
    public ExecutorService taskExecutorService() {
        int poolSize = 2;
        ExecutorService executorService = Executors.newFixedThreadPool(poolSize);
        for (int i = 0; i < poolSize; i++) {
            executorService.submit(new TaskExecutor(taskHandlerFactory));
        }
        return executorService;
    }

}
