package com.observe.ai.generic.scheduler.repository;

import com.observe.ai.generic.scheduler.model.Cron;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CronRepository extends CrudRepository<Cron, String> {
}
