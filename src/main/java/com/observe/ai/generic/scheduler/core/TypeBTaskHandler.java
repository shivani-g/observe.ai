package com.observe.ai.generic.scheduler.core;

import com.observe.ai.generic.scheduler.enums.Priority;
import com.observe.ai.generic.scheduler.enums.TaskStatus;
import com.observe.ai.generic.scheduler.enums.TaskType;
import com.observe.ai.generic.scheduler.model.Cron;
import com.observe.ai.generic.scheduler.model.Task;
import com.observe.ai.generic.scheduler.service.CronService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * To handle the task execution and context of TYPE B tasks
 */
@Component("B")
public class TypeBTaskHandler extends TaskHandler {

    @Autowired
    public CronService cronService;


    public Task create(String name,
                       TaskType type,
                       Priority priority,
                       Long duration,
                       Long executionTime,
                       String cronExpression) {

        // TODO: add validation- for duration
        Cron c = new Cron(cronExpression, name, type);
        cronService.save(c);
        return new Task(name, type, priority, duration, getNextExecutionTime(c, null));
    }

    /**
     * On status change to ACTIVE, update next execution time
     * @param task
     * @param status
     * @return task with updated status and execution context
     */
    public Task changeStatus(Task task, TaskStatus status) {
        Optional<Cron> c = cronService.findByTaskId(task.getId());
        if (c.isPresent()) {
            task.setStatus(status);
            if(task.getStatus().equals(TaskStatus.ACTIVE)) {
                task.setNextExecutionTimestamp(getNextExecutionTime(c.get(), task.getNextExecutionTimestamp()));
            }
        } else {
            //TODO: throw exception
        }
        return task;
    }


    public void postTaskExecution(Task t) {
        changeStatus(t, t.getStatus());
    }


    /**
     * Skips missed runs
     * @param c
     * @param previousExecutionTimestamp
     * @return
     */
    private Long getNextExecutionTime(Cron c, Long previousExecutionTimestamp) {
        Long now = DateTime.now().getMillis();
        Long checkAfter;
        if(previousExecutionTimestamp != null && c.findDifference(previousExecutionTimestamp) == 0){
            checkAfter = c.getRecurringPeriod();
        }
        else {
            checkAfter = c.findDifference(now);
        }
        // skipping missed runs
        return now + checkAfter;

//        return previousExecutionTime + (long) Math.ceil((now - previousExecutionTime) / (float) c.getRecurringPeriod()) * c.getRecurringPeriod();

    }

    protected void delete(Task t) {
        // do nothing
    }
}
