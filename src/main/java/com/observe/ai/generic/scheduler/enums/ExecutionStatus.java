package com.observe.ai.generic.scheduler.enums;


public enum ExecutionStatus {
    SUCCESS, FAILURE
}
