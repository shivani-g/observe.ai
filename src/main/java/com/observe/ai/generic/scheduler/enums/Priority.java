package com.observe.ai.generic.scheduler.enums;

import lombok.Getter;

@Getter
public enum Priority {
    LOW(3), MEDIUM(2), HIGH(1);

    private Integer val;

    Priority(Integer val) {
        this.val = val;
    }
}