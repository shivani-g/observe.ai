package com.observe.ai.generic.scheduler.repository;

import com.observe.ai.generic.scheduler.model.Execution;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExecutionRepository extends CrudRepository<Execution, Long> {

    List<Execution> findByEndBetween(Long t1, Long t2);
}
