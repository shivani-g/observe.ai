package com.observe.ai.generic.scheduler.core;

import com.observe.ai.generic.scheduler.enums.TaskType;


public interface TaskHandlerFactory {

    TaskHandler getTaskHandler(TaskType taskType);

}
