package com.observe.ai.generic.scheduler.enums;

public enum TaskStatus {
    ACTIVE, INACTIVE, COMPLETED
}
