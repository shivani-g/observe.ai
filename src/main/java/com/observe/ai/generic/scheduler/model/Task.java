package com.observe.ai.generic.scheduler.model;

import com.observe.ai.generic.scheduler.enums.Priority;
import com.observe.ai.generic.scheduler.enums.TaskStatus;
import com.observe.ai.generic.scheduler.enums.TaskType;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "task",
        indexes = {@Index(name = "priority_index", columnList = "priority,next_execution_timestamp")})
@ToString
@NoArgsConstructor
/**
 * Stores the task info and lock info of lock associated with the task
 */
public class Task {

    @Id
    @Column(name = "id")
    protected String id;

    @Column(name = "name")
    protected String name;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    protected TaskType type;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private TaskStatus status;

    @Column(name = "duration_in_millis")
    private Long durationInMillis;

    @Column(name = "locked_at")
    private Long lockedAt;

    @Column(name = "locked_till")
    private Long lockedTill;

    @Column(name = "next_execution_timestamp")
    private Long nextExecutionTimestamp;

    @Column(name = "locked_by")
    private Long lockedBy;

    public Task(String name,
                TaskType type,
                Priority priority,
                Long duration,
                Long executionTime) {
        this.name = name;
        this.type = type;
        this.priority = priority.getVal();
        this.durationInMillis = duration;
        this.nextExecutionTimestamp = executionTime;
        this.id = Task.getId(name, type);
        this.lockedAt = 0L;
        this.lockedTill = 0L;
        this.status = TaskStatus.ACTIVE;


    }

    public static String getId(String name, TaskType type) {
        return name + ':' + type.name();
    }


}
