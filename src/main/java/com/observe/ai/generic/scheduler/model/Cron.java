package com.observe.ai.generic.scheduler.model;

import com.observe.ai.generic.scheduler.InvalidCronException;
import com.observe.ai.generic.scheduler.enums.TaskType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@Table(name = "cron")
@ToString
@NoArgsConstructor
/**
 * Stores the cron expression, and its breakdown in the form of minute, hour and day
 */
public class Cron {

    public static final Long MINUTE = 60 * 60 * 1000L;
    public static final Long HOUR = 60 * MINUTE;
    public static final Long DAY = 24 * HOUR;

    @Id
    @Column(name = "task_id")
    protected String taskId;

    @Column(name = "expression")
    protected String expression;

    @Column(name = "minute")
    protected Integer minute;

    @Column(name = "hour")
    protected Integer hour;

    @Column(name = "day")
    protected Integer day;


    public Cron(String expression, String taskName, TaskType taskType) {
        List<String> x = Arrays.stream(expression.split(" ")).map(String::trim).collect(Collectors.toList());
        if (x.size() != 3) {
            throw new InvalidCronException("Invalid length of cron expression: " + expression);
        }
        setMinute(x.get(0));
        setHour(x.get(1));
        setDay(x.get(2));
        this.expression = expression;
        this.taskId = Task.getId(taskName, taskType);
    }

    public void setMinute(String m) {
        if (m.equals("*")) {
            this.minute = -1;
        } else if (Integer.parseInt(m) <= 59 && Integer.parseInt(m) >= 0) {
            this.minute = Integer.parseInt(m);
        } else {
            throw new InvalidCronException("Invalid minute: " + minute);
        }
    }

    public void setHour(String h) {
        if (h.equals("*")) {
            this.hour = -1;
        } else if (Integer.parseInt(h) <= 23 && Integer.parseInt(h) >= 0) {
            this.hour = Integer.parseInt(h);
        } else {
            throw new InvalidCronException("Invalid hour: " + hour);
        }
    }

    public void setDay(String d) {
        if (d.equals("*")) {
            this.day = -1;
        } else if (mapToInt(d) != -1) {
            this.minute = mapToInt(d);
        } else {
            throw new InvalidCronException("Invalid day: " + day);
        }
    }

    public Long getRecurringPeriod() {
        if (this.minute == -1) {
            return MINUTE;
        } else if (this.hour == -1) {
            return HOUR;
        } else {
            return DAY;
        }
    }


    private int mapToInt(String day) {
        int val = -1;
        switch (day) {
            case "mon":
                val = 0;
                break;
            case "tue":
                val = 1;
                break;
            case "wed":
                val = 2;
                break;
            case "thu":
                val = 3;
                break;
            case "fri":
                val = 4;
                break;
            case "sat":
                val = 5;
                break;
            case "sun":
                val = 6;
                break;

        }
        return val;
    }

    public Long findDifference(Long time) {

        DateTime t = new DateTime(time).withZone(DateTimeZone.forID("Asia/Kolkata"));
        int timeMin = t.getMinuteOfHour();
        int timeHour = t.getHourOfDay();
        int timeDay = t.getDayOfMonth();

        long difference = 0;
        if (minute != -1) {
            if (minute < timeMin) {
                minute += 60;
                if (hour != -1) {
                    hour -= 1;
                    if (hour == -1) {
                        hour = 23;
                    }
                }
            }
            difference += (minute - timeMin) * MINUTE;
        }

        if (hour != -1) {
            if (hour < timeHour) {
                hour += 24;
                if (day != -1) {
                    day -= 1;
                    if (day == -1) {
                        day = 6;
                    }
                }
            }
            difference += (hour - timeHour) * HOUR;
        }
        if (day != -1) {
            if (day < timeDay) {
                day += 7;
            }
            difference += (day - timeDay) * DAY;
        }

        return difference;
    }
}
