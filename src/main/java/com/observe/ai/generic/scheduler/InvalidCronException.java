package com.observe.ai.generic.scheduler;

public class InvalidCronException extends RuntimeException {
    public InvalidCronException(String m) {
        super(m);
    }
}
