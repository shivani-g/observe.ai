package com.observe.ai.generic.scheduler.core;

import com.observe.ai.generic.scheduler.enums.Priority;
import com.observe.ai.generic.scheduler.enums.TaskStatus;
import com.observe.ai.generic.scheduler.enums.TaskType;
import com.observe.ai.generic.scheduler.model.Execution;
import com.observe.ai.generic.scheduler.model.Task;
import com.observe.ai.generic.scheduler.service.ExecutionService;
import com.observe.ai.generic.scheduler.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class TaskContextHandler {
    @Autowired
    private TaskService taskService;

    @Autowired
    private ExecutionService executionService;

    @Autowired
    private TaskHandlerFactory taskHandlerFactory;

    /**
     * Creates and persists a new task
     * @param name
     * @param type
     * @param priority
     * @param duration
     * @param executionTime
     * @param cronExpression
     * @return created task
     */
    public Task addTask(String name,
                        TaskType type,
                        Priority priority,
                        Long duration,
                        Long executionTime,
                        String cronExpression) {
        return taskService.save(taskHandlerFactory.getTaskHandler(type).create(
                name, type, priority, duration, executionTime, cronExpression));
    }

    /**
     * For a valid input, acquires lock for the task whose status needs to be changed
     * On acquiring lock, changes status, and releases lock.
     * @param name
     * @param type
     * @param status
     * @return changed task
     */
    public Task changeStatus(String name, TaskType type, TaskStatus status) {
        Optional<Task> t = taskService.findByNameAndType(name, type);
        if(t.isPresent()) {
            Task task = t.get();
            try {
                while (!taskHandlerFactory.getTaskHandler(type).lockTask(task)) ;
                task = taskHandlerFactory.getTaskHandler(type).changeStatus(task, status);
                taskService.save(task);
            } catch (Exception ex) {
                log.error("Exception encountered while changing status of {} to {}", task.getId(), status);
            } finally {
                taskHandlerFactory.getTaskHandler(type).unlockTask(t.get());
            }
            return task;
        }
        return null;
    }

    /**
     * @return all active tasks
     */
    public List<Task> getActiveTasks() {
        return taskService.findByStatusIs(TaskStatus.ACTIVE);
    }

    /**
     * @param t1
     * @param t2
     * @return all executions between t1 and t2
     */
    public List<Execution> getExecutionsBetween(Long t1, Long t2) {
        return executionService.findBetween(t1, t2);
    }

}
