package com.observe.ai.generic.scheduler.core;

import com.observe.ai.generic.scheduler.enums.ExecutionStatus;
import com.observe.ai.generic.scheduler.enums.Priority;
import com.observe.ai.generic.scheduler.enums.TaskStatus;
import com.observe.ai.generic.scheduler.enums.TaskType;
import com.observe.ai.generic.scheduler.model.Execution;
import com.observe.ai.generic.scheduler.model.Task;
import com.observe.ai.generic.scheduler.service.ExecutionService;
import com.observe.ai.generic.scheduler.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Slf4j
public abstract class TaskHandler {

    @Autowired
    private ExecutionService executionService;

    @Autowired
    protected TaskService taskService;

    private static final long LOCK_BUFFER = 30 * 1000;

    /**
     * @return the task with highest priority and minimum nextExecutionTime(in case of same priority).
     * The task should not be locked and its nextExecutionTime <= currentTime
     */
    public Optional<Task> getExecutable() {
        Task t = taskService.findNextExecutableTask();
        return t == null ? Optional.empty() : Optional.of(t);
    }

    /**
     * @param name
     * @param type
     * @param priority
     * @param duration
     * @param executionTime
     * @param cronExpression
     * @return task with given details
     */
    public abstract Task create(String name,
                                TaskType type,
                                Priority priority,
                                Long duration,
                                Long executionTime,
                                String cronExpression);

    /**
     * @param task
     * @param status
     * @return task with changed status
     */
    public abstract Task changeStatus(Task task, TaskStatus status);


    /**
     * @param tId
     * @return Execution
     * @see Execution
     */
    public Execution execute(String tId) {
        Optional<Task> task = taskService.findById(tId);
        log.info("Starting execution of task: {}", task.get().toString());
        Long start = DateTime.now().getMillis();
        ExecutionStatus status = _execute(task.get());
        Long end = DateTime.now().getMillis();
        log.info("Completed execution of task: {}", task.get().toString());
        return executionService.create(task.get(), status, start, end,
                Thread.currentThread().getId());

    }

    /**
     * Simulates task execution via Thread.sleep(d), where d is the duration of the task
     * @param t
     * @return ExecutionStatus
     * @see ExecutionStatus
     */
    private ExecutionStatus _execute(Task t) {
        ExecutionStatus status = ExecutionStatus.SUCCESS;
        try {
            Thread.sleep(t.getDurationInMillis());
        } catch (Exception ex) {
            log.error("Exception caught while running task: {}", t.toString(), ex);
            status = ExecutionStatus.FAILURE;
        } finally {
            postTaskExecution(t);
        }
        return status;
    }


    /**
     * Includes Task status update for type A task, nextExecutionTime update for type B task
     * @param t
     */
    protected abstract void postTaskExecution(Task t);


    protected abstract void delete(Task t);


    /**
     * A task is locked using 3 values: lockedAt, lockedTill, lockedBy. MySQL update query is used for this purpose,
     * lockedAt = now
     * a condition that lockedTill < lockedAt and status is ACTIVE for the given task id.
     * @param t
     * @return true if number of updated rows > 0 else false
     */
    public Boolean lockTask(Task t) {
        log.info("inside lockTask {}, {}", Thread.currentThread().getName(), t);
        Long lockedAt = DateTime.now().getMillis();
        int updatedRows = 0;
        try {
            log.info("{} trying to obtain lock for task: {}", Thread.currentThread().getName(), t);
            updatedRows = taskService.acquireLock(lockedAt,
                    lockedAt + t.getDurationInMillis() + LOCK_BUFFER, Thread.currentThread().getId(), t.getId());
            log.info("{} after trying to acquire lock for task: {}, rows changed:{}", Thread.currentThread().getName(), t, updatedRows);
        } catch (Throwable e) {
            log.error("error in acquiring lock", e);
        }
        if (updatedRows > 0) {
            log.info("{} Successfully acquired lock for task: {}", Thread.currentThread().getName(), t);
            return Boolean.TRUE;
        }
        log.info("{} Failed to acquire lock for {}", Thread.currentThread().getName(), t);
        return Boolean.FALSE;
    }


    /**
     * A task is unlocked by setting the lockedTill value to current time if this task was locked by the same thread.
     * @param t
     * @return true if number of updated rows > 0 else false
     */
    public Boolean unlockTask(Task t) {
        log.info("inside unlock {}, {}", Thread.currentThread().getName(), t);
        int updatedRows = taskService.unlock(DateTime.now().getMillis(), t.getId(), Thread.currentThread().getId());
        if (updatedRows > 0) {
            log.info("{} unlocked task: {}", Thread.currentThread().getName(), t);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

}
