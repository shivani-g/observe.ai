package com.observe.ai.generic.scheduler.core;

import com.observe.ai.generic.scheduler.enums.TaskType;
import com.observe.ai.generic.scheduler.model.Task;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Slf4j
public class TaskExecutor implements Runnable {

    private TaskHandlerFactory taskHandlerFactory;
    private static final TaskType DEFAULT_TASK_TYPE = TaskType.A;

    public TaskExecutor(TaskHandlerFactory taskHandlerFactory) {
        this.taskHandlerFactory = taskHandlerFactory;
    }


    /**
     * Queries the task list for an executable task. On obtaining one, acquires lock for the same,
     * executes it and releases lock
     */
    @Override
    public void run() {

        while (true) {
            Optional<Task> t = Optional.empty();
            while (!t.isPresent()) {
                t = taskHandlerFactory.getTaskHandler(DEFAULT_TASK_TYPE).getExecutable();
            }
            try {
                if(taskHandlerFactory.getTaskHandler(t.get().getType()).lockTask(t.get())) {
                    taskHandlerFactory.getTaskHandler(t.get().getType()).execute(t.get().getId());
                }
            } catch (Throwable throwable) {
                log.error("Exception while executing task");
            } finally {
                taskHandlerFactory.getTaskHandler(t.get().getType()).unlockTask(t.get());
            }
        }
    }
}
