package com.observe.ai.generic.scheduler.service;

import com.observe.ai.generic.scheduler.model.Cron;
import com.observe.ai.generic.scheduler.repository.CronRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CronService {

    @Autowired
    private CronRepository cronRepository;

    public Optional<Cron> findByTaskId(String taskId) {
        return cronRepository.findById(taskId);
    }

    public Cron save(Cron cron) {
        return cronRepository.save(cron);
    }

}
