package com.observe.ai.generic.scheduler;

import com.observe.ai.generic.scheduler.core.TaskHandlerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    @Bean("typeHandlerFactory")
    public FactoryBean serviceLocatorFactoryBean() {
        ServiceLocatorFactoryBean factoryBean = new ServiceLocatorFactoryBean();
        factoryBean.setServiceLocatorInterface(TaskHandlerFactory.class);
        return factoryBean;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

    }
}
