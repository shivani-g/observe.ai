package com.observe.ai.generic.scheduler.repository;

import com.observe.ai.generic.scheduler.enums.TaskStatus;
import com.observe.ai.generic.scheduler.model.Task;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<Task, String> {

    @Modifying(clearAutomatically = true)
    @Query("update Task set lockedAt = :lockedAt, lockedTill = :lockedTill, lockedBy = :lockedBy " +
            "where lockedTill < :lockedAt and id = :id")
    int acquireLock(@Param("lockedAt") Long lockedAt,
                    @Param("lockedTill") Long lockedTill,
                    @Param("lockedBy") Long lockedBy,
                    @Param("id") String id);

    @Modifying(clearAutomatically = true)
    @Query("update Task set lockedTill = :now where id = :id and lockedBy = :unlockBy")
    int unLock(@Param("now") Long now,
               @Param("id") String id,
               @Param("unlockBy") Long unlockBy);

    List<Task> findByStatusIs(TaskStatus status);


    Task findTopByStatusIsAndLockedTillLessThanAndNextExecutionTimestampLessThanEqualOrderByPriorityAscNextExecutionTimestampAsc(TaskStatus status, Long currentTime1, Long currentTime2);
}
