package com.observe.ai.generic.scheduler.service;

import com.observe.ai.generic.scheduler.enums.TaskStatus;
import com.observe.ai.generic.scheduler.enums.TaskType;
import com.observe.ai.generic.scheduler.model.Task;
import com.observe.ai.generic.scheduler.repository.TaskRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    public Optional<Task> findByNameAndType(String name, TaskType type) {
        return findById(Task.getId(name, type));
    }

    public Optional<Task> findById(String tId) {
        return taskRepository.findById(tId);
    }

    public Task save(Task t) {
        return taskRepository.save(t);
    }

    public Task findNextExecutableTask() {
        Long now = DateTime.now().getMillis();
        return taskRepository.findTopByStatusIsAndLockedTillLessThanAndNextExecutionTimestampLessThanEqualOrderByPriorityAscNextExecutionTimestampAsc(
                TaskStatus.ACTIVE, now, now);
    }

    public List<Task> findByStatusIs(TaskStatus status) {
        return taskRepository.findByStatusIs(status);
    }

    @Transactional
    public int acquireLock(Long lockedAt, Long lockedTill, Long lockedBy, String taskId) {
        return taskRepository.acquireLock(lockedAt,
                lockedAt + lockedTill, lockedBy, taskId);
    }

    @Transactional
    public int unlock(Long now, String taskId, Long unlockBy) {
        return taskRepository.unLock(now, taskId, unlockBy);
    }
}
