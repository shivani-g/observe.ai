package com.observe.ai.generic.scheduler.core;

import com.observe.ai.generic.scheduler.enums.Priority;
import com.observe.ai.generic.scheduler.enums.TaskStatus;
import com.observe.ai.generic.scheduler.enums.TaskType;
import com.observe.ai.generic.scheduler.model.Task;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * To handle the task execution and context of TYPE A tasks
 */
@Component("A")
@Primary
public class TypeATaskHandler extends TaskHandler {

    public Task create(String name,
                       TaskType type,
                       Priority priority,
                       Long duration,
                       Long executionTime,
                       String cronExpression) {

        // TODO: add validation
        return new Task(name, type, priority, duration, executionTime);
    }

    public Task changeStatus(Task task, TaskStatus status) {
        task.setStatus(status);
        return taskService.save(task);
    }

    public void postTaskExecution(Task t) {
        t.setStatus(TaskStatus.COMPLETED);
        taskService.save(t);
        delete(t);
    }

    protected void delete(Task t) {
        // do nothing
    }
}
