package com.observe.ai.generic.scheduler.model;

import com.observe.ai.generic.scheduler.enums.ExecutionStatus;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "execution",
        indexes = {@Index(name = "end_time", columnList = "end")})
@ToString
/**
 * Stores information of an executed task, such as task id, execution start and end times, executed by
 * and the execution status
 * @see ExecutionStatus
 */
public class Execution {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "task_id")
    protected String taskId;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private ExecutionStatus status;

    @Column(name = "start")
    private Long start;

    @Column(name = "end")
    private Long end;

    @Column(name = "run_by")
    private Long runBy;
}
