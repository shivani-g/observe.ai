package com.observe.ai.generic.scheduler.service;

import com.observe.ai.generic.scheduler.enums.ExecutionStatus;
import com.observe.ai.generic.scheduler.model.Execution;
import com.observe.ai.generic.scheduler.model.Task;
import com.observe.ai.generic.scheduler.repository.ExecutionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExecutionService {

    @Autowired
    private ExecutionRepository executionRepository;

    public Execution create(Task t, ExecutionStatus status, Long start, Long end, Long runBy) {
        return save(Execution.builder()
                .taskId(t.getId())
                .status(status)
                .start(start)
                .end(end)
                .runBy(runBy).build());
    }

    public List<Execution> findBetween(Long t1, Long t2) {
        return executionRepository.findByEndBetween(t1, t2);
    }

    private Execution save(Execution execution) {
        return executionRepository.save(execution);
    }
}
