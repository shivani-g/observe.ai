package com.observe.ai.generic.scheduler.controller;

import com.observe.ai.generic.scheduler.core.TaskContextHandler;
import com.observe.ai.generic.scheduler.enums.Priority;
import com.observe.ai.generic.scheduler.enums.TaskStatus;
import com.observe.ai.generic.scheduler.enums.TaskType;
import com.observe.ai.generic.scheduler.model.Execution;
import com.observe.ai.generic.scheduler.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TaskController {

    @Autowired
    private TaskContextHandler taskContextHandler;


    @RequestMapping(value = "add", method = RequestMethod.POST)
    public Task addTask(@RequestParam("name") String name,
                        @RequestParam("type") TaskType type,
                        @RequestParam("duration") Long duration,
                        @RequestParam("priority") Priority priority,
                        @RequestParam("executionTime") Long executionTime,
                        @RequestParam("cronExpression") String cronExpression) {
        return taskContextHandler.addTask(name, type, priority, duration, executionTime, cronExpression);
    }

    @RequestMapping(value = "change-status", method = RequestMethod.PUT)
    public Task changeStatus(@RequestParam("name") String name,
                             @RequestParam("type") TaskType type,
                             @RequestParam("status") TaskStatus status) {
        return taskContextHandler.changeStatus(name, type, status);
    }

    @RequestMapping(value = "active-task/all", method = RequestMethod.GET)
    public List<Task> getActive() {
        return taskContextHandler.getActiveTasks();
    }

    @RequestMapping(value = "execution", method = RequestMethod.GET)
    public List<Execution> getActive(@RequestParam("t1") Long t1,
                                     @RequestParam("t2") Long t2) {
        return taskContextHandler.getExecutionsBetween(t1, t2);
    }
}
