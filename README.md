# GENERIC TASK SCHEDULER

## Requirements
* MySQL
* Java

### To Run
* to build: 
    * ./gradlew clean build
* to run: 
    * ./gradlew clean bootRun 
    * runs at port 8080 by default

### Guide
Sample Endpoints:
1. add task
* curl -X POST \
  'http://localhost:8080/add?name=T4&type=A&duration=3000&priority=HIGH&executionTime=1591946091000&cronExpression=' \
  -H 'Postman-Token: 3a8cec51-8221-490e-acf1-da30824377bd' \
  -H 'cache-control: no-cache'
  
2. change status
* curl -X PUT \
  'http://localhost:8080/change-status?name=T1&type=A&status=INACTIVE' \
  -H 'Postman-Token: ed50f488-dd3a-46dc-ab2c-4779e1cdc9ea' \
  -H 'cache-control: no-cache'
  
3. Query: get all active tasks
* curl -X GET \
  http://localhost:8080/active-task/all \
  -H 'Postman-Token: 873a7c4d-4430-4423-8a7f-c988460a029e' \
  -H 'cache-control: no-cache'
  
4. Query: get all executions between timestamps: t1, t2
* curl -X GET \
  'http://localhost:8080/execution?t1=1591852500000&t2=1592025300000' \
  -H 'Postman-Token: a40f9ee2-7bbf-40a5-b371-76554afdcf55' \
  -H 'cache-control: no-cache'
